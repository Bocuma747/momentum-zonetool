import sys
import os
import shutil
import threading
MODULES_PATH = os.path.abspath(os.path.join(os.getcwd(), "zonetool/"))
sys.path.insert(0, MODULES_PATH)
import configparser
import insertzones
import vbsp
import makevmf
from tkinter import Tk, filedialog, ttk, font, messagebox, scrolledtext, Grid, Toplevel, IntVar, StringVar, Menu

CONFIG_PATH = os.path.abspath(os.path.join(os.getcwd(), "etc/config.ini"))
TEMP_VMF_PATH = os.path.abspath(os.path.join(os.getcwd(), "temp.vmf"))


def make_new_config():
    with open(CONFIG_PATH, 'w+') as configfile:
        config = configparser.ConfigParser()
        config.add_section('Paths')
        config.set('Paths', 'VBSP_path', 'Not set')
        config.set('Paths', 'GameDir', 'Not set')
        config.write(configfile)

def update_config(section, item, newvalue):
    with open(CONFIG_PATH, 'r+') as configfile:
        config = configparser.ConfigParser()
        config.read(CONFIG_PATH)
        config.set(section, item, newvalue)
        config.write(configfile)

def runthread(target):
    thread = threading.Thread(target=target)
    thread.start()


class Window_Console:
    def __init__(self, master):

        def copyall():
            self.textbox_contents = self.textbox.get("1.0", 'end-1c')
            root.clipboard_clear()
            root.clipboard_append(self.textbox_contents)

        self.errorcount = 0
        self.warningcount = 0
        self.errorslist = []
        self.warningslist = []

        self.master = master
        master.title("Insert zone -- Zone To BSP")
        Grid.rowconfigure(master, 0, weight=1)
        Grid.columnconfigure(master, 0, weight=1)
        Grid.columnconfigure(master, 1, weight=1)

        self.textbox = scrolledtext.ScrolledText(master,
                                                 wrap="word",
                                                 state="disabled")
        self.textbox.grid(row=0, column=0, columnspan=2, sticky="nsew")
        self.textbox.tag_configure("green", foreground="green")
        self.textbox.tag_configure("red", foreground="red")
        self.textbox.tag_configure("orange", foreground="orange")
        self.textbox.tag_configure("black", foreground="black")

        self.button_copylog = ttk.Button(master,
                                         text="Copy All",
                                         command=copyall)
        self.button_copylog.grid(row=1, column=0, sticky="nsew")

        self.button_closewindow = ttk.Button(master,
                                             text="Close",
                                             command=master.destroy)
        self.button_closewindow.grid(row=1, column=1, sticky="nsew")

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        self.initwindowsize = "%dx%d" % (self.screenwidth/2,
                                         self.screenheight/1.5)
        master.geometry(self.initwindowsize)

    def output(self, message, flag):
        if flag == "n":     # "n" = normal
            color = "black"
        elif flag == "g":   # "g" = green text
            color = "green"
        elif flag == "w":   # "w" = warning
            color = "orange"
            self.warningcount += 1
            self.warningslist.append(message)
        elif flag == "e":   # "e" = error
            color = "red"
            self.errorcount += 1
            self.errorslist.append(message)
        # Set color to black if an invalid flag is given
        else:
            color = "black"

        # Write the message to textbox
        self.textbox.configure(state="normal")
        self.textbox.insert("end", message, color)
        self.textbox.see("end")
        self.textbox.configure(state="disabled")


class Main:
    def __init__(self, master):

        self.bsps_list = []
        self.zones_list = []
        # Make a config file if it's missing from the cwd/etc folder.
        if not os.path.isfile(CONFIG_PATH):
            print("No config found; making one.")
            make_new_config()
            self.vbsppath = None
            self.gamedir = None
        # Otherwise, read it and get the values.
        else:
            self.config = configparser.ConfigParser()
            self.config.read(CONFIG_PATH)
            if self.config['Paths']['VBSP_path'] == 'Not set':
                self.vbsppath = None
            else:
                self.vbsppath = self.config['Paths']['VBSP_path']
            if self.config['Paths']['GameDir'] == 'Not set':
                self.gamedir = None
            else:
                self.gamedir = self.config['Paths']['GameDir']

        def update_tree_lists():
            self.bsps_list = []
            self.zones_list = []
            for item in self.tree_files.get_children():
                self.bsps_list.append(os.path.splitext(os.path.basename(self.tree_files.set(item)['BSPs']))[0])
                self.zones_list.append(os.path.splitext(os.path.basename(self.tree_files.set(item)['Zones']))[0])

        def openfile():
            filepaths = filedialog.askopenfilename(title="Select file(s)",
                                                   filetypes=[("Map and zone files", "*.bsp *.zon *.vmf")],
                                                   multiple=True, parent=master)
            for filepath in filepaths:
                # Update lists of items in each column
                update_tree_lists()

                file_name = os.path.splitext(os.path.basename(filepath))[0]
                file_ext = os.path.splitext(os.path.basename(filepath))[1]

                if file_ext == ".bsp":
                    if file_name not in self.zones_list:
                        self.tree_files.insert('', 'end', values=(os.path.abspath(filepath), ''))
                    else:
                        for item in self.tree_files.get_children():
                            if os.path.splitext(os.path.basename(self.tree_files.set(item)["Zones"]))[0] == file_name:
                                self.tree_files.set(item, column="BSPs", value=os.path.abspath(filepath))

                elif (file_ext == ".zon") or (file_ext == ".vmf"):
                    if file_name not in self.bsps_list:
                        self.tree_files.insert('', 'end', values=('', os.path.abspath(filepath)))
                    else:
                        for item in self.tree_files.get_children():
                            if os.path.splitext(os.path.basename(self.tree_files.set(item)["BSPs"]))[0] == file_name:
                                self.tree_files.set(item, column="Zones", value=os.path.abspath(filepath))

        def zontovmf():
            filepath = filedialog.askopenfilename(title="Select .zon to convert to .vmf",
                                                  filetypes=[("Momentum .zon files", "*.zon")],
                                                  multiple=False, parent=master)
            if filepath:
                dir = os.path.dirname(filepath)
                filename = os.path.splitext(os.path.basename(filepath))[0]
                with open(filepath, 'r') as file:
                    makevmf.MakeVMF(zon=file.read(), outputdir=os.path.join(dir, filename+".vmf"))
                    messagebox.showinfo('.zon to .vmf', "%s.vmf created at %s/%s.vmf" % (filename, dir, filename))

        def choosevbsppath():
            self.vbsppath_dialog = filedialog.askopenfilename(title="Select vbsp.exe", filetypes=[("vbsp.exe", "vbsp.exe")], parent=master)
            if self.vbsppath_dialog == "":
                pass
            else:
                self.vbsppath = self.vbsppath_dialog
                update_config('Paths', 'VBSP_path', self.vbsppath)
                self.labeltext_VBSP_Path.set(self.vbsppath)

        def choosegamedir():
            self.gamedir_dialog = filedialog.askdirectory(title="Select GameDir directory", parent=master)
            if self.gamedir_dialog == "":
                pass
            else:
                if "gameinfo.txt" not in os.listdir(self.gamedir_dialog):
                    messagebox.showerror(
                        "Error",
                        "gameinfo.txt not found in GameDir!\nGameDir is the \"cstrike\" folder for CS:S\nand \"momentum\" for Momentum.\nhttps://developer.valvesoftware.com/wiki/Game_Directory",
                        parent=master)
                else:
                    self.gamedir = self.gamedir_dialog
                    update_config('Paths', 'GameDir', self.gamedir)
                    self.labeltext_GameDir.set(self.gamedir)

        def removeselected(tree):
            selection = tree.selection()
            for i in selection[::-1]:
                tree.delete(i)
            update_tree_lists()

        def run():
            self.newwindow = Toplevel(self.master)
            consolewindow = Window_Console(self.newwindow)
            ERRORCOUNT = 0
            FAILED_MAPS = []

            for tree_item in self.tree_files.get_children():
                item_values = self.tree_files.set(tree_item)
                zonefile_path = item_values['Zones']
                bsp_path = item_values['BSPs']

                if zonefile_path == '':
                    consolewindow.output("\nNo zone file chosen for map %s; skipping.\n\n" % bsp_path, "e")
                    pass
                elif bsp_path == '':
                    consolewindow.output("\nNo bsp file chosen for zone file %s; skipping.\n\n" % zonefile_path, "e")
                    pass
                elif not os.path.exists(zonefile_path):
                    consolewindow.output("\nMap failed: Path %s does not exist. Skipping.\n\n" % zonefile_path, "e")
                    pass
                elif not os.path.exists(bsp_path):
                    consolewindow.output("\nMap failed: Path %s does not exist. Skipping.\n\n" % bsp_path, "e")
                    pass
                else:
                    try:
                        zonefile_name = os.path.splitext(os.path.basename(zonefile_path))[0]
                        zonefile_ext = os.path.splitext(os.path.basename(zonefile_path))[1]
                        bsp_name = os.path.splitext(os.path.basename(bsp_path))[0]

                        consolewindow.output(("\n======================= "
                                              "Inserting %s into %s..."
                                              "=======================\n") % (os.path.basename(zonefile_path), os.path.basename(bsp_path)), "n")

                        # Make a backup of the bsp if the option to do so is selected.
                        if self.var_makebackup.get() == 1:
                            shutil.copy(bsp_path, os.path.join(os.path.dirname(bsp_path), bsp_name+".bsp.backup"))
                            consolewindow.output("Made backup of %s.bsp in %s\n\n" % (bsp_name, os.path.dirname(bsp_path)), "g")

                        with open(zonefile_path, "r") as zonefile:
                            with open(bsp_path, "rb+") as bspfile:
                                # If the zone file is a .zon file,
                                # create temp.vmf which contains the zones. The file will be in the CWD.
                                if zonefile_ext == ".zon":
                                    consolewindow.output("Converting %s to .VMF...\n\n" % zonefile_path, "n")
                                    makevmf.MakeVMF(zon=zonefile.read(), outputdir=TEMP_VMF_PATH)
                                # If the zone file is a .vmf,
                                # then copy it to CWD/temp.vmf and use that instead.
                                elif zonefile_ext == ".vmf":
                                    # Copy it to cwd so that vbsp doesn't overwrite the bsp if it happens to be in the same directory.
                                    consolewindow.output("Copying %s to temporary .VMF...\n\n" % zonefile_path, "n")
                                    shutil.copy(zonefile_path, TEMP_VMF_PATH)

                                # Compile temp.vmf with vbsp.
                                consolewindow.output("VBSP - Compiling zones...\n", "n")
                                vbsp_inst = vbsp.VBSP(vbsppath=self.vbsppath,
                                                      gamedir=self.gamedir,
                                                      vmfpath=TEMP_VMF_PATH,
                                                      console=consolewindow)
                                vbsp_inst.run()

                                # Write the new bsp.
                                consolewindow.output("Inserting zones into %s.bsp...\n" % bsp_name, "n")
                                insertzones_instance = insertzones.InsertZones(output=bspfile.read(),
                                                                               console=consolewindow)
                                final_bsp = insertzones_instance.writezones()
                                consolewindow.output("Done.\n\n", "n")
                                consolewindow.output("Writing new .bsp to %s\n" % bsp_path, "n")
                                with open(bsp_path, 'wb') as outputfile:
                                    outputfile.write(final_bsp)
                                consolewindow.output("Finished writing %s\n\n" % bsp_path, "g")

                        consolewindow.output("Map %s.bsp done.\n\n" % bsp_name, "g")

                    except Exception as e:
                        consolewindow.output("\n======== Exception raised: ========\n\n%s\n\nMap %s.bsp failed!\n" % (e, bsp_name), "e")
                        ERRORCOUNT += 1
                        FAILED_MAPS.append(bsp_name)
                        pass

            consolewindow.output("\n\n------------------\nJob done.\n", "g")
            if ERRORCOUNT > 0:
                consolewindow.output("\n%d map(s) encountered errors and may not have been zoned correctly:\n" % ERRORCOUNT, "e")
                for map in FAILED_MAPS:
                    consolewindow.output("  %s.bsp\n" % map, "e")
            consolewindow.output("\nYou may now close this window.", "g")

        # GUI
        self.master = master
        master.title("Zone To BSP")
        Grid.rowconfigure(master, 2, weight=1)
        Grid.columnconfigure(master, 1, weight=1)
        self.style = ttk.Style()
        self.fontSegoe11 = font.Font(family="Segoe UI", size=11)
        self.style.configure("Size11.TButton", font=self.fontSegoe11)

        self.menubar = Menu(master)
        self.file_menu = Menu(self.menubar, tearoff=0)
        self.file_menu.add_command(label="Convert .zon to .vmf...", command=lambda: runthread(zontovmf))
        self.menubar.add_cascade(label="File", menu=self.file_menu)

        ttk.Label(master, text="Select .BSP map files.\nThen select Momentum Mod .zon files, or .vmf map files containing only the zone triggers.\nA zone file (.zon or .vmf) must have the same name as the BSP it will be inserted into.").grid(row=0, column=0, columnspan=3, sticky="w", padx=10, pady=(20,0))

        ttk.Button(master,
                   text="Open...",
                   command=openfile).grid(row=1, column=0, columnspan=3, sticky="w", padx=10, pady=(15,20))

        self.tree_files = ttk.Treeview(master, columns=('BSPs', 'Zones'), show='headings')
        self.tree_files.heading("#1", text="BSPs")
        self.tree_files.heading("#2", text="Zones")
        self.tree_files.grid(row=2, column=0, columnspan=2, sticky="nsew", padx=(10, 0))

        self.sb_x_1 = ttk.Scrollbar(master, command=self.tree_files.xview, orient="horizontal")
        self.sb_x_1.grid(row=3, column=0, columnspan=2, sticky="ew", padx=10)
        self.sb_y_1 = ttk.Scrollbar(master, command=self.tree_files.yview)
        self.sb_y_1.grid(row=2, column=2, sticky="ns", padx=(0, 10))
        self.tree_files.configure(xscrollcommand=self.sb_x_1.set, yscrollcommand=self.sb_y_1.set)

        ttk.Button(master,
                   text="Remove Selected",
                   command=lambda: removeselected(self.tree_files)).grid(row=4, column=0, columnspan=2, sticky="e", padx=10, pady=20)

        ttk.Button(master,
                   text="Choose vbsp.exe path",
                   command=choosevbsppath).grid(row=5, column=0, columnspan=3, sticky="w", padx=10, pady=10)

        self.labeltext_VBSP_Path = StringVar()
        if self.vbsppath is not None:
            self.labeltext_VBSP_Path.set(self.vbsppath)
        self.label_VBSP_Path = ttk.Label(master,
                                         text="",
                                         textvariable=self.labeltext_VBSP_Path,
                                         background="white",
                                         style="TLabel").grid(row=6, column=0, columnspan=2, sticky="nsew", padx=10, pady=10)

        ttk.Button(master,
                   text="Choose GameDir path",
                   command=choosegamedir).grid(row=7, column=0, columnspan=3, sticky="w", padx=10, pady=10)

        self.labeltext_GameDir = StringVar()
        if self.gamedir is not None:
            self.labeltext_GameDir.set(self.gamedir)
        self.label_GameDir = ttk.Label(master,
                                       text="",
                                       textvariable = self.labeltext_GameDir,
                                       background="white",
                                       style="TLabel").grid(row=8, column=0, columnspan=2, sticky="nsew", padx=10, pady=10)

        self.var_makebackup = IntVar()
        self.var_makebackup.set(1)
        self.checkbutton_Makebackup = ttk.Checkbutton(master,
                                                      text="Backup original .bsp files",
                                                      variable=self.var_makebackup)
        self.checkbutton_Makebackup.grid(row=9, column=0, sticky='w', padx=10, pady=5)

        ttk.Separator(orient="horizontal").grid(row=10, column=0, columnspan=3, sticky="ew", pady=10)

        ttk.Button(master,
                   text="Go",
                   command=lambda: runthread(target=run)).grid(row=11, column=0, columnspan=2, sticky="ns", padx=20, pady=(10, 20))

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        self.initwindowsize = "%dx%d" % (self.screenwidth/3, self.screenheight/1.2)
        master.geometry(self.initwindowsize)
        master.config(menu=self.menubar)


if __name__ == '__main__':
    root = Tk()
    RUN_GUI = Main(root)
    root.iconbitmap(os.path.abspath(os.path.join(os.getcwd(), "etc/icon.ico")))
    root.mainloop()
