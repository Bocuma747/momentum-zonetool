import bspmodule
import readentlump
import WriteBSPFile
import os
import struct
import io


# Having to .seek() every time I write bytes is annoying so this func does it for me.
def WriteBytes(bytesobject, bytes):
    bytesobject.write(bytes)
    bytesobject.seek(0, 2)


class InsertZonesException(Exception):
    pass


class InsertZones:
    '''Pass this the raw bytes of the bsp we want to write to.'''
    def __init__(self, output, console):
        self.input = open(os.path.join(os.getcwd(), "temp.bsp"), 'rb').read()
        self.output = output
        self.console = console
        self.read_temp = bspmodule.BSP(self.input)
        self.read_main = bspmodule.BSP(self.output)
        self.bsp_version = self.read_main.get_version()

    def writezones(self):
        # PLANE LUMP (#1):
        lump_from_temp = self.read_temp.get_lump(1)
        lump_from_main = self.read_main.get_lump(1)
        PLANECOUNT = len(lump_from_main) // 20
        print("%d planes" % PLANECOUNT)
        # Just slap the new lump onto the old one.
        newlump_plane = lump_from_main + lump_from_temp



        # VERTEX LUMP (#3):
        lump_from_temp = self.read_temp.get_lump(3)
        lump_from_main = self.read_main.get_lump(3)
        VERTEXCOUNT = len(lump_from_main) // 12
        print("%d vertices" % VERTEXCOUNT)
        # Don't add the first 12 bytes (the first vertex) of the temp lump.
        # The first vertex is always (0, 0, 0) so it throws off the array if we don't remove it.
        newlump_vertex = lump_from_main + lump_from_temp[12:]



        #TexdataStringData (#43):
        lump_from_main = self.read_main.get_lump(43)
        lump_from_temp = self.read_temp.get_lump(43)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        texdatastringdata_offset = len(lump_from_main)
        #newlump_texdatastringdata = lump_from_main + b'TOOLS/TOOLSTRIGGER\x00'
        newlump_texdatastringdata = lump_from_main + lump_from_temp




        #TexdataStringTable (#44)
        lump_from_main = self.read_main.get_lump(44)
        lump_from_temp = self.read_temp.get_lump(44)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEXDATASTRINGTABLE_COUNT = len(lump_from_main) // 4
        TEXDATASTRINGTABLE_COUNT_TEMP = len(lump_from_temp) // 4
        print("%d TexDataStringTable entries" % TEXDATASTRINGTABLE_COUNT)

        texdatastringtable_temp_list = []
        while True:
            offset_bytes = lump_from_temp_bytesobj.read(4)
            if len(offset_bytes) != 4:
                break
            else:
                offset_int = struct.unpack('i', offset_bytes)[0]
                texdatastringtable_temp_list.append(offset_int + texdatastringdata_offset)
        new_templump = io.BytesIO()
        for offset in texdatastringtable_temp_list:
            WriteBytes(new_templump, struct.pack('i', offset))
#        newlump_texdatastringtable = lump_from_main + struct.pack('i', texdatastringdata_offset)
        newlump_texdatastringtable = lump_from_main + new_templump.getvalue()




        # TEXDATA LUMP (#2):
        lump_from_main = self.read_main.get_lump(2)
        lump_from_temp = self.read_temp.get_lump(2)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEXDATACOUNT = len(lump_from_main) // 32
        TEXDATACOUNT_TEMP = len(lump_from_temp) // 32
        print("%d texdatas" % TEXDATACOUNT)
        new_texdatalump = io.BytesIO()

        for i in range(0, TEXDATACOUNT_TEMP):
            texdata_unpacked = struct.unpack('3f5i', lump_from_temp_bytesobj.read(32))
            texdata_reflectivity = [texdata_unpacked[0], texdata_unpacked[1], texdata_unpacked[2]]
            texdata_nameStringTableID = texdata_unpacked[3]
            texdata_width = texdata_unpacked[4]
            texdata_height = texdata_unpacked[5]
            texdata_viewwidth = texdata_unpacked[6]
            texdata_viewheight = texdata_unpacked[7]
            new_nameStringTableID = texdata_nameStringTableID + TEXDATASTRINGTABLE_COUNT

            for reflectivity in texdata_reflectivity:
                WriteBytes(new_texdatalump, struct.pack('f', reflectivity))
            WriteBytes(new_texdatalump, struct.pack('i', new_nameStringTableID))
            for size in [texdata_width, texdata_height, texdata_viewwidth, texdata_viewheight]:
                WriteBytes(new_texdatalump, struct.pack('i', size))

        newlump_texdata = lump_from_main + new_texdatalump.getvalue()



        # TEXINFO LUMP (#6):
        lump_from_main = self.read_main.get_lump(6)
        lump_from_temp = self.read_temp.get_lump(6)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEXINFOCOUNT = len(lump_from_main) // 72
        TEXINFOCOUNT_TEMP = len(lump_from_temp) // 72
        print("%d texinfos" % TEXINFOCOUNT)
        new_texinfo = io.BytesIO()

        for i in range(0, TEXINFOCOUNT_TEMP):
            texinfo_unpacked = struct.unpack('16f2i', lump_from_temp_bytesobj.read(72))
            texinfo_texdataindex = texinfo_unpacked[17]
            new_texdataindex = texinfo_texdataindex + TEXDATACOUNT
            for item in texinfo_unpacked[0:16]:
                WriteBytes(new_texinfo, struct.pack('f', item))
            WriteBytes(new_texinfo, struct.pack('i', texinfo_unpacked[16]))
            WriteBytes(new_texinfo, struct.pack('i', new_texdataindex))

        newlump_texinfo = lump_from_main + new_texinfo.getvalue()



        # EDGE LUMP (#12):
        lump_from_temp = self.read_temp.get_lump(12)
        lump_from_main = self.read_main.get_lump(12)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        EDGECOUNT = len(lump_from_main) // 4
        EDGECOUNT_TEMP = len(lump_from_temp) // 4
        print("%d edges" % EDGECOUNT)
        new_edgelump = io.BytesIO()

        for i in range(0, EDGECOUNT_TEMP):
            edge_unpacked = struct.unpack('HH', lump_from_temp_bytesobj.read(4))
            new_vertex1 = VERTEXCOUNT + edge_unpacked[0] - 1
            new_vertex2 = VERTEXCOUNT + edge_unpacked[1] - 1
            WriteBytes(new_edgelump, struct.pack('H', new_vertex1))
            WriteBytes(new_edgelump, struct.pack('H', new_vertex2))

        newlump_edge = lump_from_main + new_edgelump.getvalue()[4:]



        # SURFEDGE LUMP (#13):
        lump_from_temp = self.read_temp.get_lump(13)
        lump_from_main = self.read_main.get_lump(13)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        SURFEDGECOUNT = len(lump_from_main) // 4
        SURFEDGECOUNT_TEMP = len(lump_from_temp) // 4
        print("%d surfedges" % SURFEDGECOUNT)
        frmtstr = '%di' % SURFEDGECOUNT_TEMP
        surfedgelump_unpacked = struct.unpack(frmtstr, lump_from_temp)
        new_surfedgelump = io.BytesIO()

        for surfedge in surfedgelump_unpacked:
            if surfedge >= 0:
                new_surfedge = (surfedge + EDGECOUNT) - 1
            elif surfedge < 0:
                new_surfedge = ((abs(surfedge) + EDGECOUNT) * -1) + 1
            WriteBytes(new_surfedgelump, struct.pack('i', new_surfedge))

        newlump_surfedge = lump_from_main + new_surfedgelump.getvalue()



        # ORIGINAL FACE LUMP (#27):
        lump_from_temp = self.read_temp.get_lump(27)
        lump_from_main = self.read_main.get_lump(27)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEMP_ORIGFACECOUNT = len(lump_from_temp) // 56
        ORIGFACECOUNT = len(lump_from_main) // 56
        print("%d original faces" % ORIGFACECOUNT)
        new_origfacelump = io.BytesIO()

        for i in range(0, TEMP_ORIGFACECOUNT):
            new_origfacelump.seek(0, 2)
            # Get the data from this origface structure
            origface_unpacked = struct.unpack('H??ihhhh????ifiiiiiHHI', lump_from_temp_bytesobj.read(56))
            origface_planenum = origface_unpacked[0]
            origface_side = origface_unpacked[1]
            origface_onNode = origface_unpacked[2]
            origface_firstedge = origface_unpacked[3]
            origface_numedges = origface_unpacked[4]
            origface_texinfo = origface_unpacked[5]
            origface_dispinfo = origface_unpacked[6]
            origface_surorigfaceFogVolumeID = origface_unpacked[7]
            origface_styles = [origface_unpacked[8], origface_unpacked[9], origface_unpacked[10], origface_unpacked[11]]
            origface_lightofs = origface_unpacked[12]
            origface_area = origface_unpacked[13]
            origface_LightmapTextureMinsInLuxels = [origface_unpacked[14], origface_unpacked[15]]
            origface_LightmapTextureSizeInLuxels = [origface_unpacked[16], origface_unpacked[17]]
            origface_origface = origface_unpacked[18]
            origface_numPrims = origface_unpacked[19]
            origface_firstPrimID = origface_unpacked[20]
            origface_smoothingGroups = origface_unpacked[21]

            new_planenum = PLANECOUNT + origface_planenum
            new_firstedge = SURFEDGECOUNT + origface_firstedge
            new_texinfo = TEXINFOCOUNT + origface_texinfo

            WriteBytes(new_origfacelump, struct.pack('H', new_planenum))
            WriteBytes(new_origfacelump, struct.pack('b', origface_side))
            WriteBytes(new_origfacelump, struct.pack('b', origface_onNode))
            WriteBytes(new_origfacelump, struct.pack('i', new_firstedge))
            WriteBytes(new_origfacelump, struct.pack('h', origface_numedges))
            WriteBytes(new_origfacelump, struct.pack('h', new_texinfo))
            WriteBytes(new_origfacelump, struct.pack('h', origface_dispinfo))
            WriteBytes(new_origfacelump, struct.pack('h', origface_surorigfaceFogVolumeID))
            for style in origface_styles:
                WriteBytes(new_origfacelump, struct.pack('b', style))
            WriteBytes(new_origfacelump, struct.pack('i', origface_lightofs))
            WriteBytes(new_origfacelump, struct.pack('f', origface_area))
            for min in origface_LightmapTextureMinsInLuxels:
                WriteBytes(new_origfacelump, struct.pack('i', min))
            for size in origface_LightmapTextureSizeInLuxels:
                WriteBytes(new_origfacelump, struct.pack('i', size))
            WriteBytes(new_origfacelump, struct.pack('i', origface_origface))
            WriteBytes(new_origfacelump, struct.pack('H', origface_numPrims))
            WriteBytes(new_origfacelump, struct.pack('H', origface_firstPrimID))
            WriteBytes(new_origfacelump, struct.pack('I', origface_smoothingGroups))

        newlump_origface = lump_from_main + new_origfacelump.getvalue()



        # FACE LUMP (#7):
        lump_from_temp = self.read_temp.get_lump(7)
        lump_from_main = self.read_main.get_lump(7)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEMP_FACECOUNT = len(lump_from_temp) // 56
        FACECOUNT = len(lump_from_main) // 56
        print("%d faces" % FACECOUNT)
        new_facelump = io.BytesIO()

        for i in range(0, TEMP_FACECOUNT):
            new_facelump.seek(0, 2)
            # Get the data from this face structure
            face_unpacked = struct.unpack('HbbihhhhbbbbifiiiiiHHI', lump_from_temp_bytesobj.read(56))
            face_planenum = face_unpacked[0]
            face_side = face_unpacked[1]
            face_onNode = face_unpacked[2]
            face_firstedge = face_unpacked[3]
            face_numedges = face_unpacked[4]
            face_texinfo = face_unpacked[5]
            face_dispinfo = face_unpacked[6]
            face_surfaceFogVolumeID = face_unpacked[7]
            face_styles = [face_unpacked[8], face_unpacked[9], face_unpacked[10], face_unpacked[11]]
            face_lightofs = face_unpacked[12]
            face_area = face_unpacked[13]
            face_LightmapTextureMinsInLuxels = [face_unpacked[14], face_unpacked[15]]
            face_LightmapTextureSizeInLuxels = [face_unpacked[16], face_unpacked[17]]
            face_origFace = face_unpacked[18]
            face_numPrims = face_unpacked[19]
            face_firstPrimID = face_unpacked[20]
            face_smoothingGroups = face_unpacked[21]

            new_planenum = PLANECOUNT + face_planenum
            new_firstedge = SURFEDGECOUNT + face_firstedge
            new_texinfo = TEXINFOCOUNT + face_texinfo
            new_origFace = ORIGFACECOUNT + face_origFace

            WriteBytes(new_facelump, struct.pack('H', new_planenum))
            WriteBytes(new_facelump, struct.pack('b', face_side))
            WriteBytes(new_facelump, struct.pack('b', face_onNode))
            WriteBytes(new_facelump, struct.pack('i', new_firstedge))
            WriteBytes(new_facelump, struct.pack('h', face_numedges))
            WriteBytes(new_facelump, struct.pack('h', new_texinfo))
            WriteBytes(new_facelump, struct.pack('h', face_dispinfo))
            WriteBytes(new_facelump, struct.pack('h', face_surfaceFogVolumeID))
            for style in face_styles:
                WriteBytes(new_facelump, struct.pack('b', style))
            WriteBytes(new_facelump, struct.pack('i', face_lightofs))
            WriteBytes(new_facelump, struct.pack('f', face_area))
            for min in face_LightmapTextureMinsInLuxels:
                WriteBytes(new_facelump, struct.pack('i', min))
            for size in face_LightmapTextureSizeInLuxels:
                WriteBytes(new_facelump, struct.pack('i', size))
            WriteBytes(new_facelump, struct.pack('i', new_origFace))
            WriteBytes(new_facelump, struct.pack('H', face_numPrims))
            WriteBytes(new_facelump, struct.pack('H', face_firstPrimID))
            WriteBytes(new_facelump, struct.pack('I', face_smoothingGroups))

        newlump_face = lump_from_main + new_facelump.getvalue()



        # BRUSHSIDE LUMP (#19):
        lump_from_temp = self.read_temp.get_lump(19)
        lump_from_main = self.read_main.get_lump(19)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        BRUSHSIDECOUNT = len(lump_from_main) // 8
        TEMP_BRUSHSIDECOUNT = len(lump_from_temp) // 8
        print("%d brushsides" % BRUSHSIDECOUNT)
        new_brushsidelump = io.BytesIO()

        for i in range(0, TEMP_BRUSHSIDECOUNT):
            brushside_unpacked = struct.unpack('Hhhh', lump_from_temp_bytesobj.read(8))
            brushside_planenum = brushside_unpacked[0]
            brushside_texinfo = brushside_unpacked[1]
            brushside_dispinfo = brushside_unpacked[2]
            brushside_bevel = brushside_unpacked[3]
            new_planenum = PLANECOUNT + brushside_planenum
            new_texinfo = TEXINFOCOUNT + brushside_texinfo
            WriteBytes(new_brushsidelump, struct.pack('Hhhh', new_planenum,
                                                              new_texinfo,
                                                              brushside_dispinfo,
                                                              brushside_bevel))

        newlump_brushside = lump_from_main + new_brushsidelump.getvalue()



        # BRUSH LUMP (#18):
        lump_from_temp = self.read_temp.get_lump(18)
        lump_from_main = self.read_main.get_lump(18)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        BRUSHCOUNT = len(lump_from_main) // 12
        TEMP_BRUSHCOUNT = len(lump_from_temp) // 12
        print("%d brushes" % BRUSHCOUNT)
        new_brushlump = io.BytesIO()

        for i in range(0, TEMP_BRUSHCOUNT):
            brush_unpacked = struct.unpack('3i', lump_from_temp_bytesobj.read(12))
            brush_firstside = brush_unpacked[0]
            brush_numsides = brush_unpacked[1]
            brush_contents = brush_unpacked[2]
            new_firstside = BRUSHSIDECOUNT + brush_firstside - 1
            WriteBytes(new_brushlump, struct.pack('3i', new_firstside,
                                                        brush_numsides,
                                                        brush_contents))

        newlump_brush = lump_from_main + new_brushlump.getvalue()



        # LEAFFACE LUMP (#16):
        lump_from_temp = self.read_temp.get_lump(16)
        lump_from_main = self.read_main.get_lump(16)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        LEAFFACECOUNT = len(lump_from_main) // 2
        TEMP_LEAFFACECOUNT = len(lump_from_temp) // 2
        print("%d leaffaces" % LEAFFACECOUNT)
        frmtstr = '%dH' % TEMP_LEAFFACECOUNT
        new_leaffacelump = io.BytesIO()

        for leafface in struct.unpack(frmtstr, lump_from_temp):
            WriteBytes(new_leaffacelump, struct.pack('H', leafface + FACECOUNT))

        newlump_leafface = lump_from_main + new_leaffacelump.getvalue()



        # LEAFBRUSH LUMP (#17):
        lump_from_temp = self.read_temp.get_lump(17)
        lump_from_main = self.read_main.get_lump(17)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        LEAFBRUSHCOUNT = len(lump_from_main) // 2
        TEMP_LEAFBRUSHCOUNT = len(lump_from_temp) // 2
        print("%d leafbrushes" % LEAFBRUSHCOUNT)
        frmtstr = '%dH' % TEMP_LEAFBRUSHCOUNT
        new_leafbrushlump = io.BytesIO()

        for leafbrush in struct.unpack(frmtstr, lump_from_temp):
            WriteBytes(new_leafbrushlump, struct.pack('H', leafbrush + BRUSHCOUNT))

        newlump_leafbrush = lump_from_main + new_leafbrushlump.getvalue()



        # LEAF LUMP (#10):
        lump_from_temp = self.read_temp.get_lump(10)
        lump_from_main = self.read_main.get_lump(10)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        if self.bsp_version == 20:
            LEAFCOUNT = len(lump_from_main) // 32
            TEMP_LEAFCOUNT = len(lump_from_temp) // 32
        elif self.bsp_version == 19:
            LEAFCOUNT = len(lump_from_main) // 56
            TEMP_LEAFCOUNT = len(lump_from_temp) // 56
        print("%d leafs" % LEAFCOUNT)
        new_leaflump = io.BytesIO()

        for i in range(0, TEMP_LEAFCOUNT):
            if self.bsp_version == 20:
                leaf_contents = struct.unpack('i', lump_from_temp_bytesobj.read(4))
                leaf_cluster = struct.unpack('h', lump_from_temp_bytesobj.read(2))
                leaf_bitfield = lump_from_temp_bytesobj.read(2)
                leaf_mins = struct.unpack('3h', lump_from_temp_bytesobj.read(6))
                leaf_maxs = struct.unpack('3h', lump_from_temp_bytesobj.read(6))
                leaf_firstleafface = struct.unpack('H', lump_from_temp_bytesobj.read(2))
                leaf_numleaffaces = struct.unpack('H', lump_from_temp_bytesobj.read(2))
                leaf_firstleafbrush = struct.unpack('H', lump_from_temp_bytesobj.read(2))
                leaf_numleafbrushes = struct.unpack('H', lump_from_temp_bytesobj.read(2))
                leaf_leafWaterDataID = struct.unpack('h', lump_from_temp_bytesobj.read(2))
                leaf_padding = lump_from_temp_bytesobj.read(2)
                new_firstleafface = LEAFFACECOUNT + leaf_firstleafface[0]
                new_firstleafbrush = LEAFBRUSHCOUNT + leaf_firstleafbrush[0]

                WriteBytes(new_leaflump, struct.pack('i', leaf_contents[0]))
                WriteBytes(new_leaflump, struct.pack('h', leaf_cluster[0]))
                WriteBytes(new_leaflump, leaf_bitfield)
                for min in leaf_mins:
                    WriteBytes(new_leaflump, struct.pack('h', min))
                for max in leaf_maxs:
                    WriteBytes(new_leaflump, struct.pack('h', max))
                WriteBytes(new_leaflump, struct.pack('4H', new_firstleafface,
                                                           leaf_numleaffaces[0],
                                                           new_firstleafbrush,
                                                           leaf_numleafbrushes[0]))
                WriteBytes(new_leaflump, struct.pack('h', leaf_leafWaterDataID[0]))
                WriteBytes(new_leaflump, leaf_padding)
            elif self.bsp_version == 19:
                raise InsertZonesException("If you get this message, remind Bocuma to add support for v19 bsps and call him a lazy prick")

        newlump_leaf = lump_from_main + new_leaflump.getvalue()



        # NODE LUMP (#5):
        new_planenum = PLANECOUNT
        lump_from_temp = self.read_temp.get_lump(5)
        lump_from_main = self.read_main.get_lump(5)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEMP_NODECOUNT = len(lump_from_temp) // 32
        NODECOUNT = len(lump_from_main) // 32
        print("%d nodes" % NODECOUNT)
        new_nodelump = io.BytesIO()

        for i in range(0, TEMP_NODECOUNT):
            new_nodelump.seek(0, 2)
            # Get the data from this node structure
            node_unpacked = struct.unpack('3i6h2H2h', lump_from_temp_bytesobj.read(32))
            node_planenum = node_unpacked[0]
            node_child1 = node_unpacked[1]
            node_child2 = node_unpacked[2]
            node_mins = [node_unpacked[3], node_unpacked[4], node_unpacked[5]]
            node_maxs = [node_unpacked[6], node_unpacked[7], node_unpacked[8]]
            node_firstface = node_unpacked[9]
            node_numfaces = node_unpacked[10]
            node_area = node_unpacked[11]
            node_padding = node_unpacked[12]
            new_planenum = PLANECOUNT + node_planenum

            # Child #1
            # If the child is a node index:
            if node_child1 >= 0:
                new_child1 = node_child1 + NODECOUNT
            # If the child is a leaf index:
            elif node_child1 < 0:
                old_leafnum1 = -1 - node_child1
                new_leafnum1 = old_leafnum1 + LEAFCOUNT
                new_child1 = -1 * new_leafnum1

            # Child #2
            # If the child is a node index:
            if node_child2 >= 0:
                new_child2 = node_child2 + NODECOUNT
            # If the child is a leaf index:
            elif node_child2 < 0:
                old_leafnum2 = -1 - node_child2
                new_leafnum2 = old_leafnum2 + LEAFCOUNT
                new_child2 = -1 * new_leafnum2

            new_firstface = node_firstface + FACECOUNT

            WriteBytes(new_nodelump, struct.pack('i', new_planenum))
            WriteBytes(new_nodelump, struct.pack('i', new_child1))
            WriteBytes(new_nodelump, struct.pack('i', new_child2))
            for min in node_mins:
                WriteBytes(new_nodelump, struct.pack('h', min))
            for max in node_maxs:
                WriteBytes(new_nodelump, struct.pack('h', max))
            WriteBytes(new_nodelump, struct.pack('H', new_firstface))
            WriteBytes(new_nodelump, struct.pack('H', node_numfaces))
            WriteBytes(new_nodelump, struct.pack('h', node_area))
            WriteBytes(new_nodelump, struct.pack('h', node_padding))

        newlump_node = lump_from_main + new_nodelump.getvalue()



        # MODEL LUMP (#14):
        lump_from_temp = self.read_temp.get_lump(14)
        lump_from_main = self.read_main.get_lump(14)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        lump_from_temp_bytesobj.seek(0, 0)
        TEMP_MODELCOUNT = len(lump_from_temp) // 48
        MODELCOUNT = len(lump_from_main) // 48
        print("%d models" % MODELCOUNT)
        new_modellump = io.BytesIO()

        for i in range(0, TEMP_MODELCOUNT):
            new_modellump.seek(0, 2)
            # Get the data from this model structure
            model_unpacked = struct.unpack('9f3i', lump_from_temp_bytesobj.read(48))
            model_mins = [model_unpacked[0], model_unpacked[1], model_unpacked[2]]
            model_maxs = [model_unpacked[3], model_unpacked[4], model_unpacked[5]]
            model_origin = [model_unpacked[6], model_unpacked[7], model_unpacked[8]]
            model_headnode = model_unpacked[9]
            model_firstface = model_unpacked[10]
            model_numfaces = model_unpacked[11]
            new_headnode = NODECOUNT + model_headnode
            new_firstface = FACECOUNT + model_firstface
            for min in model_mins:
                WriteBytes(new_modellump, struct.pack('f', min))
            for max in model_maxs:
                WriteBytes(new_modellump, struct.pack('f', max))
            for origin_coord in model_origin:
                WriteBytes(new_modellump, struct.pack('f', origin_coord))
            WriteBytes(new_modellump, struct.pack('i', new_headnode))
            WriteBytes(new_modellump, struct.pack('i', new_firstface))
            WriteBytes(new_modellump, struct.pack('i', model_numfaces))

        newlump_model = lump_from_main + new_modellump.getvalue()



        # ENTITY LUMP (#0):
        new_modelnum = MODELCOUNT
        lump_from_temp = self.read_temp.get_lump(0)
        lump_from_main = self.read_main.get_lump(0)
        new_entlump = ""
        parseentlump = readentlump.ReadEntLump(lump_from_temp)
        entlump = parseentlump.parse()
        TEMP_ENTITYCOUNT = len(entlump) - 1     # Subtract 1 because we're skipping the worldspawn entity.
        self.console.output("   Adding entities to entity lump...\n", "n")

        for ent in entlump:
            # Skip the worldspawn entity
            if '"classname" "worldspawn"' not in ent:
                new_entlump += "{\n"
                for keyval in ent:
                    if keyval[:10] == '"model" "*':
                        new_entlump += '"model" "*{modelnum}"\n'.format(modelnum = new_modelnum)
                        new_modelnum += 1
                    else:
                        if keyval[:11] == '"classname"':
                            self.console.output("       Added %s.\n" % keyval[13:-1], "n")
                        new_entlump += keyval+"\n"
                new_entlump += "}\n"

        newlump_entity = lump_from_main.strip("\x00".encode('ascii')) + new_entlump.strip().encode('ascii')



        # PHYSCOLLIDE LUMP (#29):
        lump_from_main = self.read_main.get_lump(29)
        lump_from_temp = self.read_temp.get_lump(29)
        lump_from_temp_bytesobj = io.BytesIO(lump_from_temp)
        new_templump = io.BytesIO()
        # The main bsp already has a terminating structure so we need to skip over that, hence the [:-16].
        if struct.unpack('4i', lump_from_main[-16:]) == (-1, -1, 0, 0):
            while True:
                physcollide_header = lump_from_temp_bytesobj.read(16)
                # When we reach the end, break.
                if physcollide_header == '':
                    break
                else:
                    physcollide_header_unpack = struct.unpack('iiii', physcollide_header)
                    modelIndex = physcollide_header_unpack[0]
                    # Keep the final model index as is, as this is the terminating structure.
                    if modelIndex == -1:
                        WriteBytes(new_templump, struct.pack('iiii', -1, -1, 0, 0))
                        break
                    else:
                        new_modelindex = modelIndex + MODELCOUNT
                        dataSize = physcollide_header_unpack[1]
                        keydataSize = physcollide_header_unpack[2]
                        solidCount = physcollide_header_unpack[3]
                        # print("\nmodelindex: %d,\ndatasize: %d,\nkeydataSize: %d,\nsolidCount: %d\n\n" % (new_modelindex, dataSize, keydataSize, solidCount))
                        # TODO: skip worldspawn physcollide if necessary
                        collision_data = lump_from_temp_bytesobj.read(dataSize + keydataSize)
                        WriteBytes(new_templump, struct.pack('iiii', new_modelindex, dataSize, keydataSize, solidCount))
                        WriteBytes(new_templump, collision_data)
            newlump_physcollide = lump_from_main[:-16] + new_templump.getvalue()

        else:
            print("Original map has a physcollide lump without a terminating structure; this should literally never happen and if it does it probably creates an intergalactic portal somewhere.")



        # Write the list of lumps.
        lumpslist = []
        for i in range(0, 64):
            if i == 0:
                lumpslist.append(newlump_entity)
                self.console.output("   Inserted %d entities.\n" % TEMP_ENTITYCOUNT, "n")
            elif i == 1:
                lumpslist.append(newlump_plane)
            elif i == 2:
                lumpslist.append(newlump_texdata)
            elif i == 3:
                lumpslist.append(newlump_vertex)
            elif i == 5:
                lumpslist.append(newlump_node)
            elif i == 6:
                lumpslist.append(newlump_texinfo)
            elif i == 7:
                lumpslist.append(newlump_face)
            elif i == 10:
                lumpslist.append(newlump_leaf)
            elif i == 12:
                lumpslist.append(newlump_edge)
            elif i == 13:
                lumpslist.append(newlump_surfedge)
            elif i == 14:
                lumpslist.append(newlump_model)
                self.console.output("   Inserted %d models.\n" % TEMP_MODELCOUNT, "n")
            elif i == 16:
                lumpslist.append(newlump_leafface)
            elif i == 17:
                lumpslist.append(newlump_leafbrush)
            elif i == 18:
                lumpslist.append(newlump_brush)
                self.console.output("   Inserted %d brushes.\n" % TEMP_BRUSHCOUNT, "n")
            elif i == 19:
                lumpslist.append(newlump_brushside)
            elif i == 27:
                lumpslist.append(newlump_origface)
            elif i == 29:
                lumpslist.append(newlump_physcollide)
            elif i == 43:
                lumpslist.append(newlump_texdatastringdata)
            elif i == 44:
                lumpslist.append(newlump_texdatastringtable)
            else:
                lumpslist.append(self.read_main.get_lump(i))

        writebsp = WriteBSPFile.WriteBSPFile(lumpslist, self.bsp_version)
        the_whole_fucking_bsp_lol = writebsp.write()

        if os.path.isfile(os.path.abspath(os.path.join(os.getcwd(), "temp.bsp"))):
            os.remove(os.path.abspath(os.path.join(os.getcwd(), "temp.bsp")))

        return the_whole_fucking_bsp_lol
