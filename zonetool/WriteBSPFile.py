import struct
import io


# Having to .seek() every time I write bytes is annoying so this func does it for me.
def WriteBytes(bytesobject, bytes):
    bytesobject.write(bytes)
    bytesobject.seek(0, 2)


def GetLumpVersion(bspversion, lump):
    '''Pass this the BSP version and the lump number and it'll return the lump version.'''
    if bspversion == 19:
        if lump in {7, 8}:
            lumpversion = 1
        elif lump == 9:
            lumpversion = 2
        else:
            lumpversion = 0
    elif bspversion == 20:
        if lump in {7, 8, 10, 53, 55, 56}:
            lumpversion = 1
        elif lump == 9:
            lumpversion = 2
        else:
            lumpversion = 0
    return lumpversion


def FixGameLump(lump, offset):
    ''' We have to make a special modification to the Game Lump (lump #35).
    It has its own set of offset headers that need to be changed if the length or position
    of any lump before it changes.'''
    gamelump = io.BytesIO(lump)
    gamelump.seek(0, 0)
    new_gamelump = io.BytesIO()

    # Write the gamelump count, the number of lumps within the gamelump.
    gamelumpcount = struct.unpack('i', gamelump.read(4))[0]
    WriteBytes(new_gamelump, struct.pack('i', gamelumpcount))

    ##### Write the first gamelump directory array #####
    # Write the first gamelump ID, the flags, and the gamelump version (8 bytes total).
    WriteBytes(new_gamelump, gamelump.read(8))
    # Write the new offset of the first gamelump.
    # This is the whole gamelump's offset + 4 bytes (the gamelump count) + each gamelump header (16 bytes each).
    gamelump_offset = offset + 4 + (gamelumpcount * 16)
    WriteBytes(new_gamelump, struct.pack('i', gamelump_offset))
    # Skip past the old offset.
    gamelump.read(4)
    # Write the gamelump's size.
    gamelump_size = gamelump.read(4)
    new_gamelump.write(gamelump_size)

    ##### Write the rest of the gamelump directory arrays #####
    for gamelumpnum in range(gamelumpcount - 1):
        new_gamelump.seek(0, 2)
        # Write the first gamelump ID, the flags & the gamelump version (8 bytes total).
        WriteBytes(new_gamelump, gamelump.read(8))
        # Write the new offset of this gamelump from the start of the bsp.
        # This is the previous gamelump's offset + the previous gamelump's size.
        gamelump_offset += struct.unpack('i', gamelump_size)[0]
        WriteBytes(new_gamelump, struct.pack('i', gamelump_offset))
        # Skip past the old offset
        gamelump.read(4)
        # Write the gamelump's size
        gamelump_size = gamelump.read(4)
        new_gamelump.write(gamelump_size)

    # Write the actual game lumps.
    new_gamelump.seek(0, 2)
    new_gamelump.write(gamelump.read())
    return new_gamelump.getvalue()


class WriteBSPFileException(Exception):
    pass


class WriteBSPFile:
    '''Pass this a list of all the bsp's lumps. The list should have one item per lump,
    each item being the raw bytes of that lump, and they must be in the correct order.'''
    def __init__(self, lumps, version):
        self.lumps = lumps
        self.version = version
        if self.version not in {19, 20}:
            raise WriteBSPFileException("Unsupported BSP version: %d" % self.version)
        if not isinstance(self.lumps, list):
            raise WriteBSPFileException("Bad object type, must be a list")
        elif len(self.lumps) != 64:
            raise WriteBSPFileException("List contains %d items (must contain 64)" % len(self.lumps))

    def write(self):
        # This will be the 1036 byte header at the start of the file.
        headerBytes = io.BytesIO()
        # This will be everything in the file after the header.
        mapBytes = io.BytesIO()

        # Start by writing the identifier (the letters "VBSP").
        WriteBytes(headerBytes, "VBSP".encode('ascii'))
        # Write the bsp version number.
        WriteBytes(headerBytes, struct.pack('i', self.version))

        # Write the lump_t structure for each lump to the header,
        # then write the lump itself.
        for lumpnum, lump in enumerate(self.lumps):
            LUMP_OFFSET = 1036 + len(mapBytes.getvalue())
            LUMP_LEN = len(lump)
            LUMP_VERSION = GetLumpVersion(self.version, lumpnum)
            WriteBytes(headerBytes, struct.pack('i', LUMP_OFFSET))
            WriteBytes(headerBytes, struct.pack('i', LUMP_LEN))
            WriteBytes(headerBytes, struct.pack('i', LUMP_VERSION))
            WriteBytes(headerBytes, struct.pack('4b', 0, 0, 0, 0))
            # Write the lump.
            if lumpnum == 35:
                # If this lump is the gamelump, pass it to FixGameLump() first.
                WriteBytes(mapBytes, FixGameLump(lump, LUMP_OFFSET))
            else:
                # Otherwise, just write it.
                WriteBytes(mapBytes, lump)

        # Write the map revision number; unknown so we'll just write 1.
        WriteBytes(headerBytes, struct.pack('i', 1))

        # Now we write the final bsp.
        output = io.BytesIO()
        # Write the header and the lumps to output.
        WriteBytes(output, headerBytes.getvalue())
        WriteBytes(output, mapBytes.getvalue())

        return output.getvalue()
