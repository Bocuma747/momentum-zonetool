import struct
import io


class ReadEntLump:
    def __init__(self, file):
        self.file = file.decode(errors='ignore')

    def parse(self):
        """Returns a list containing one list per entity.
        Each of those lists containins that entity's keyvalues.
        """
        self.lines = []
        self.in_block = False
        # I use the terms "prop" and "keyvalue" interchangeably.
        self.props = []
        self.lumpblocks = []

        # Read the entity lump's lines and make a list out of them.
        self.lumpfile = self.file.split("\n")
        for line in self.lumpfile:
            # Skip null lines
            if line == "\x00":
                continue
            # Strip \n and null chars from line and append it to the list.
            else:
                self.lines.append(line.strip("\n\x00"))

        # Now iterate through our list of lines.
        for line in self.lines:
            # When we find the start of a new block, clear the props (keyvalues) list.
            if line == "{" and self.in_block is False:
                self.in_block = True
                self.props = []
            # When we find the end of a block,
            # flush the props list to the lumpblocks list and clear the props list.
            elif line == "}" and self.in_block:
                self.in_block = False
                self.lumpblocks.append(self.props)
                self.props = []
            # If we're in a block and we find a line, then it's a keyvalue.
            # Append it to props.
            elif self.in_block:
                self.props.append(line)

        return self.lumpblocks
