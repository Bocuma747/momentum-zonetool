import os
import zonfile


U_V_AXES = [("[1 0 0 0] 0.5", "[0 -1 0 0] 0.5"),
            ("[1 0 0 0] 0.5", "[0 1 0 0] 0.5"),
            ("[0 -1 0 0] 0.5", "[0 0 -1 0] 0.5"),
            ("[0 1 0 0] 0.5", "[0 0 -1 0] 0.5"),
            ("[1 0 0 0] 0.5", "[0 0 -1 0] 0.5"),
            ("[1 0 0 0] 0.5", "[0 0 -1 0] 0.5")]

VMF_STRING = """versioninfo
{
    "editorversion" "400"
    "editorbuild" "7538"
    "mapversion" "1"
	"formatversion" "100"
	"prefab" "0"
}
visgroups
{
}
viewsettings
{
	"bSnapToGrid" "1"
	"bShowGrid" "1"
	"bShowLogicalGrid" "0"
	"nGridSpacing" "64"
	"bShow3DGrid" "0"
}
world
{
	"id" "1"
	"mapversion" "1"
	"classname" "worldspawn"
	"detailmaterial" "detail/detailsprites"
	"detailvbsp" "detail.vbsp"
	"maxpropscreenwidth" "-1"
	"skyname" "sky_day01_01"
}
"""


class MakeVMF:
    def __init__(self, zon, outputdir):
        ZonFile = zonfile.ZonFile(zon)
        self.zones = ZonFile.get_parsed_zones()
        self.vmf = VMF_STRING
        self.outputdir = outputdir

        SOLID_AND_SIDE_ID = 9999
        for zonenum, zone in enumerate(self.zones):
            ENT_ID = zonenum + 1
            ZONETYPE = zone['TYPE']
            ORIGIN = "%s %s %s" % (zone["xpos"], zone["ypos"], zone["zpos"])
            ANGLES = "%s %s %s" % (zone["xRot"], zone["yRot"], zone["zRot"])

            VERTEX1 = [float(zone["xpos"])+float(zone["xScaleMins"]),
                       float(zone["ypos"])+float(zone["yScaleMins"]),
                       float(zone["zpos"])+float(zone["zScaleMins"])]
            VERTEX1_STR = "%f %f %f" % (VERTEX1[0], VERTEX1[1], VERTEX1[2])

            VERTEX2 = [float(zone["xpos"])+float(zone["xScaleMins"]),
                       float(zone["ypos"])+float(zone["yScaleMaxs"]),
                       float(zone["zpos"])+float(zone["zScaleMaxs"])]
            VERTEX2_STR = "%f %f %f" % (VERTEX2[0], VERTEX2[1], VERTEX2[2])

            VERTEX3 = [float(zone["xpos"])+float(zone["xScaleMaxs"]),
                       float(zone["ypos"])+float(zone["yScaleMaxs"]),
                       float(zone["zpos"])+float(zone["zScaleMaxs"])]
            VERTEX3_STR = "%f %f %f" % (VERTEX3[0], VERTEX3[1], VERTEX3[2])

            VERTEX4 = [float(zone["xpos"])+float(zone["xScaleMaxs"]),
                       float(zone["ypos"])+float(zone["yScaleMins"]),
                       float(zone["zpos"])+float(zone["zScaleMaxs"])]
            VERTEX4_STR = "%f %f %f" % (VERTEX4[0], VERTEX4[1], VERTEX4[2])

            VERTEX5 = [float(zone["xpos"])+float(zone["xScaleMaxs"]),
                       float(zone["ypos"])+float(zone["yScaleMins"]),
                       float(zone["zpos"])+float(zone["zScaleMins"])]
            VERTEX5_STR = "%f %f %f" % (VERTEX5[0], VERTEX5[1], VERTEX5[2])

            VERTEX6 = [float(zone["xpos"])+float(zone["xScaleMaxs"]),
                       float(zone["ypos"])+float(zone["yScaleMaxs"]),
                       float(zone["zpos"])+float(zone["zScaleMins"])]
            VERTEX6_STR = "%f %f %f" % (VERTEX6[0], VERTEX6[1], VERTEX6[2])

            VERTEX7 = [float(zone["xpos"])+float(zone["xScaleMins"]),
                       float(zone["ypos"])+float(zone["yScaleMins"]),
                       float(zone["zpos"])+float(zone["zScaleMaxs"])]
            VERTEX7_STR = "%f %f %f" % (VERTEX7[0], VERTEX7[1], VERTEX7[2])

            VERTEX8 = [float(zone["xpos"])+float(zone["xScaleMins"]),
                       float(zone["ypos"])+float(zone["yScaleMaxs"]),
                       float(zone["zpos"])+float(zone["zScaleMins"])]
            VERTEX8_STR = "%f %f %f" % (VERTEX8[0], VERTEX8[1], VERTEX8[2])

            PLANE1 = "(%s) (%s) (%s)" % (VERTEX2_STR, VERTEX3_STR, VERTEX4_STR)
            PLANE2 = "(%s) (%s) (%s)" % (VERTEX1_STR, VERTEX5_STR, VERTEX6_STR)
            PLANE3 = "(%s) (%s) (%s)" % (VERTEX2_STR, VERTEX7_STR, VERTEX1_STR)
            PLANE4 = "(%s) (%s) (%s)" % (VERTEX6_STR, VERTEX5_STR, VERTEX4_STR)
            PLANE5 = "(%s) (%s) (%s)" % (VERTEX3_STR, VERTEX2_STR, VERTEX8_STR)
            PLANE6 = "(%s) (%s) (%s)" % (VERTEX5_STR, VERTEX1_STR, VERTEX7_STR)
            PLANES = [PLANE1, PLANE2, PLANE3, PLANE4, PLANE5, PLANE6]

            if ZONETYPE == "STARTZONE":
                ENTITY = """    "id" "{ENT_ID}"
    "classname" "trigger_momentum_timer_start"
    "bhopleavespeed" "{bhopleavespeed}"
    "LimitSpeedType" "{LimitSpeedType}"
    "lookangles" "0 0 0"
    "origin" "{origin}"
    "spawnflags" "{spawnflags}"
    "StartDisabled" "0"
    "StartOnJump" "{StartOnJump}"
    "ZoneNumber" "{ZoneNumber}" """.format(ENT_ID = ENT_ID,
                                           bhopleavespeed = zone["bhopleavespeed"],
                                           LimitSpeedType = zone["LimitSpeedType"],
                                           origin = ORIGIN,
                                           spawnflags = 8193,
                                           StartOnJump = zone["StartOnJump"],
                                           ZoneNumber = zone["ZoneNumber"]
                                           )

            elif ZONETYPE == "ENDZONE":
                ENTITY = """    "id" "{ENT_ID}"
    "classname" "trigger_momentum_timer_stop"
    "origin" "{origin}"
    "spawnflags" "{spawnflags}"
    "StartDisabled" "0"
    "ZoneNumber" "{ZoneNumber}" """.format(ENT_ID = ENT_ID,
                                           origin = ORIGIN,
                                           spawnflags = 1,
                                           ZoneNumber = zone["ZoneNumber"]
                                           )

            elif ZONETYPE == "STAGEZONE":
                ENTITY = """    "id" "{ENT_ID}"
    "classname" "trigger_momentum_timer_stage"
    "origin" "{origin}"
    "spawnflags" "{spawnflags}"
    "stage" "{stagenumber}"
    "StartDisabled" "0" """.format(ENT_ID = ENT_ID,
                                   origin = ORIGIN,
                                   spawnflags = 1,
                                   stagenumber = zone["number"]
                                   )

            entstring = ("""entity
{{
{ENTITY}
    solid
    {{
        "id" "{SOLID_ID}"
""").format(ENTITY = ENTITY,
            SOLID_ID = SOLID_AND_SIDE_ID)

            self.vmf += entstring

            for planenum, plane in enumerate(PLANES):
                # Get the texture axes for this face.
                if planenum < 6:
                    u_v_list = U_V_AXES[planenum]
                else:
                    u_v_list = ("[1 0 0 0] 0.5", "[0 0 -1 0] 0.5")


                SOLID_AND_SIDE_ID += 1
                sidestring = ("""        side
        {{
            "id" "{SIDE_ID}"
            "plane" "{PLANE}"
            "material" "TOOLS/TOOLSTRIGGER"
            "uaxis" "{UAXIS}"
            "vaxis" "{VAXIS}"
            "rotation" "0"
            "lightmapscale" "128"
            "smoothing_groups" "0"
        }}
""").format(SIDE_ID = SOLID_AND_SIDE_ID,
            PLANE = plane,
            UAXIS = u_v_list[0],
            VAXIS = u_v_list[1])

                self.vmf += sidestring

            self.vmf += """        editor
        {
            "color" "220 30 220"
            "visgroupshown" "1"
            "visgroupautoshown" "1"
        }
    }
"""
            self.vmf += """    editor
    {
        "color" "220 30 220"
        "visgroupshown" "1"
        "visgroupautoshown" "1"
        "logicalpos" "[0 0]"
    }
}
"""

        # Add this to the very end of the vmf:
        self.vmf += """cameras
{
	"activecamera" "-1"
}
cordon
{
	"mins" "(-1024 -1024 -1024)"
	"maxs" "(1024 1024 1024)"
	"active" "0"
}
"""
        with open(self.outputdir, "w") as output:
            output.write(self.vmf)


    def getvalues(self):
        return self.zones
