import re

KEYVAL_REGEX = re.compile(r'\"(.*)\"\s*\"(.*)\"')
DICT_START = {
              "TYPE": None,
              "bhopleavespeed": None,
              "limitingspeed": None,
              "StartOnJump": None,
              "LimitSpeedType": None,
              "ZoneNumber": None,
              "xpos": None,
              "ypos": None,
              "zpos": None,
              "xRot": None,
              "yRot": None,
              "zRot": None,
              "xScaleMins": None,
              "yScaleMins": None,
              "zScaleMins": None,
              "xScaleMaxs": None,
              "yScaleMaxs": None,
              "zScaleMaxs": None,
              }
DICT_END = {
            "TYPE": None,
            "ZoneNumber": None,
            "xpos": None,
            "ypos": None,
            "zpos": None,
            "xRot": None,
            "yRot": None,
            "zRot": None,
            "xScaleMins": None,
            "yScaleMins": None,
            "zScaleMins": None,
            "xScaleMaxs": None,
            "yScaleMaxs": None,
            "zScaleMaxs": None,
            }
DICT_STAGE = {
              "TYPE": None,
              "number": None,
              "xpos": None,
              "ypos": None,
              "zpos": None,
              "xRot": None,
              "yRot": None,
              "zRot": None,
              "xScaleMins": None,
              "yScaleMins": None,
              "zScaleMins": None,
              "xScaleMaxs": None,
              "yScaleMaxs": None,
              "zScaleMaxs": None,
              }


class ZonFileException(Exception):
    pass


class ZonFile:
    '''Read and parse a .zon file.
    Pass the actual text from the .zon file to this, not the file object or path.'''
    def __init__(self, zon):
        '''Creates a list of dictionaries. Each dictionary contains the zone's keyvalues.'''
        self.zonefile = zon
        self.lines = []
        self.zones = []
        # Strip whitespace from lines and make a list of the lines
        for line in self.zonefile.split("\n"):
            self.lines.append(line.strip())
        # Iterate through .zon file's lines and make dictionaries out of the zones.
        for line in self.lines[2:-2]:
            # When we find a zone, choose the appropriate dict for it.
            if line == '\"start\"':
                self.THIS_DICT = {key: None for key in DICT_START}
                self.THIS_DICT['TYPE'] = "STARTZONE"
            if line == '\"end\"':
                self.THIS_DICT = {key: None for key in DICT_END}
                self.THIS_DICT['TYPE'] = "ENDZONE"
            if line == '\"stage\"':
                self.THIS_DICT = {key: None for key in DICT_STAGE}
                self.THIS_DICT['TYPE'] = "STAGEZONE"
            # When we find a keyvalue, add it to the zone's dict.
            if re.findall(KEYVAL_REGEX, line):
                keyvals = re.findall(KEYVAL_REGEX, line)[0]
                self.THIS_DICT[keyvals[0]] = keyvals[1]
            # When we reach the end of a zone, add its dict to the zones list.
            if line == "}":
                self.zones.append(self.THIS_DICT)

    def get_parsed_zones(self):
        return self.zones

    def modifyzone(self, zonenumber, key, value):
        key = str(key)
        if zonenumber > len(self.zones) - 1:
            raise ZonFileException("Zone number does not exist: %d" % zonenumber)
        else:
            zone_dict = self.zones[zonenumber]
            if key in zone_dict:
                self.zones[zonenumber][key] = str(value)
            else:
                raise ZonFileException("Key does not exist: %s" % key)
