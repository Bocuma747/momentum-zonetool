import subprocess
import os

class VBSP:
    def __init__(self, vbsppath, gamedir, vmfpath, console):
        self.vbsppath = os.path.abspath(vbsppath)
        self.gamedir = os.path.abspath(gamedir)
        self.vmfpath = os.path.abspath(vmfpath)
        self.console = console
        self.args = '"{vbsppath}" -game "{gamedir}" "{vmfpath}"'.format(vbsppath=self.vbsppath,
                                                                        gamedir=self.gamedir,
                                                                        vmfpath=self.vmfpath)

    def run(self):
        p = subprocess.Popen(self.args, universal_newlines=True, stdout=subprocess.PIPE)
        while True:
            line = p.stdout.readline().rstrip()
            if not line:
                self.console.output("VBSP done.\n\n", "n")
                break
            self.console.output("       %s\n" % line, "n")

        if os.path.isfile(os.path.abspath(os.path.join(os.getcwd(), "temp.vmf"))):
            os.remove(os.path.abspath(os.path.join(os.getcwd(), "temp.vmf")))
        if os.path.isfile(os.path.abspath(os.path.join(os.getcwd(), "temp.log"))):
            os.remove(os.path.abspath(os.path.join(os.getcwd(), "temp.log")))
